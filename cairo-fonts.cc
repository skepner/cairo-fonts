//

#include <iostream>
#include <string>
#include <vector>
#include <cstdio>
#include <cmath>
#include <getopt.h>

#include "cairo.hh"

// ----------------------------------------------------------------------

std::vector<std::string> get_available_fonts(bool monospace_only);
void draw(std::string filename, const std::vector<std::string>& columns, const std::vector<double>& column_offsets, const std::vector<std::string>& fonts, size_t canvas_height, size_t canvas_width, double font_size, double space);
std::vector<double> calculate_column_offsets(const std::vector<std::string>& columns, const std::vector<std::string>& fonts, double font_size, double space);
[[noreturn]] void usage(std::string progname);

// ----------------------------------------------------------------------

class SurfaceError : public std::runtime_error
{
 public: using std::runtime_error::runtime_error;
};

// ----------------------------------------------------------------------

class Surface
{
 private:
    class PushContext
    {
     public:
        inline PushContext(Surface& aSurface) : mContext(aSurface.mContext) { cairo_save(mContext); }
        inline ~PushContext() { cairo_restore(mContext); }

     private:
        cairo_t* mContext;
    };

 public:
    inline Surface(std::string filename, size_t width, size_t height)
        : mContext(nullptr)
        {
            auto surface = cairo_pdf_surface_create(filename.empty() ? nullptr : filename.c_str(), width, height);
            if (cairo_surface_status(surface) != CAIRO_STATUS_SUCCESS)
                throw SurfaceError("cannot create pdf surface");
            mContext = cairo_create(surface);
            cairo_surface_destroy(surface);
            if (cairo_status(mContext) != CAIRO_STATUS_SUCCESS)
                throw SurfaceError("cannot create mContext");
        }

    inline ~Surface()
        {
            if (mContext != nullptr)
                cairo_destroy(mContext);
        }

    inline void show(double aX, double aY, std::string aText, std::string aFace, double aSize)
        {
            PushContext pc(*this);
            cairo_select_font_face(mContext, aFace.c_str(), CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
            cairo_set_font_size(mContext, aSize);
            cairo_set_source_rgba(mContext, 0, 0, 0, 1);
            cairo_move_to(mContext, aX, aY);
            cairo_show_text(mContext, aText.c_str());
        }

    inline std::pair<double, double> text_size(std::string aText, std::string aFace, double aSize)
        {
            PushContext pc(*this);
            cairo_select_font_face(mContext, aFace.c_str(), CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
            cairo_set_font_size(mContext, aSize);
            cairo_text_extents_t text_extents;
            cairo_text_extents(mContext, aText.c_str(), &text_extents);
            return std::make_pair(text_extents.x_advance, - text_extents.y_bearing);
        }

 private:
    cairo_t* mContext;
    friend class PushContext;
};

// ----------------------------------------------------------------------

int main(int argc, char * const * argv)
{
    bool monospace_only = false;
    size_t canvas_height = 850;
    std::vector<std::string> columns {"The quick brown fox jumps over the lazy dog 01234567890", "sans-serif", "monospace"};
    static struct option longopts[] = {
        {"monospace", no_argument, nullptr, 'm'},
        {"height",    required_argument, nullptr, 'v'},
        {"text",    required_argument, nullptr, 't'},
          // {"daggerset",  no_argument,            &daggerset,     1 },
        {nullptr,         0,                      NULL,           0 }
    };
    int ch;
    while ((ch = getopt_long(argc, argv, "mv:t:", longopts, nullptr)) != -1) {
        switch (ch) {
          case 'm':
              monospace_only = true;
              break;
          case 'v':
              canvas_height = std::stoul(optarg);
              break;
          case 't':
              columns[0] = optarg;
              break;
          default:
              usage(argv[0]);
        }
    }
    if (argc == optind)
        usage(argv[0]);
    argc -= optind;
    argv += optind;

    auto fonts = get_available_fonts(monospace_only);
    const double font_size = std::min(static_cast<double>(canvas_height) / static_cast<double>(fonts.size()), 10.0);
    const double space = font_size * 3;
    const auto column_offsets = calculate_column_offsets(columns, fonts, font_size, space);
      // std::copy(fonts.begin(), fonts.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
    draw(argv[0], columns, column_offsets, fonts, canvas_height, static_cast<size_t>(std::ceil(column_offsets.back())), font_size, space);

} // main

// ----------------------------------------------------------------------

void usage(std::string progname)
{
    std::cerr << "Usage: " << progname << " [-m|--monospace] [-v|--height <canvas-height>] [-t|--text <text-to-show>] <output.pdf>" << std::endl;
    exit(1);

} // usage

// ----------------------------------------------------------------------

std::vector<double> calculate_column_offsets(const std::vector<std::string>& columns, const std::vector<std::string>& fonts, double font_size, double space)
{
    std::vector<double> result;
    Surface s("", 1, 1);

    double x = space;
    double max_width_1 = 0, max_width_2 = 0;
    for (size_t font_no = 1; font_no < fonts.size(); ++font_no) {
        max_width_1 = std::max(max_width_1, s.text_size(std::to_string(font_no) + " " + fonts[font_no], fonts[font_no], font_size).first);
        max_width_2 = std::max(max_width_2, s.text_size(columns[0], fonts[font_no], font_size).first);
    }
    x += max_width_1 + space;
    result.push_back(x);
    x += max_width_2 + space;
    result.push_back(x);
    x += s.text_size(columns[1], columns[1], font_size).first + space;
    result.push_back(x);
    x += s.text_size(columns[2], columns[2], font_size).first + space;
    result.push_back(x);

    return result;

} // calculate_canvas_width

// ----------------------------------------------------------------------

void draw(std::string filename, const std::vector<std::string>& columns, const std::vector<double>& column_offsets, const std::vector<std::string>& fonts, size_t canvas_height, size_t canvas_width, double font_size, double space)
{
    Surface s(filename, canvas_width, canvas_height);

    double x = space;
    double y = font_size;
    size_t font_no = 1;
    for (const auto& font: fonts) {
        const auto t = std::to_string(font_no) + " " + font;
        s.show(x, y, t, font, font_size);
        y += font_size;
        ++font_no;
    }

    for (size_t col = 0; col < columns.size(); ++col) {
        y = font_size;
        for (const auto& font: fonts) {
            s.show(column_offsets[col], y, columns[col], col == 0 ? font : columns[col], font_size);
            y += font_size;
        }
    }
}

// ----------------------------------------------------------------------

std::vector<std::string> get_available_fonts(bool monospace_only)
{
    std::vector<std::string> fonts;
    std::string cmd = std::string("fc-list :lang=en") + (monospace_only ? ":spacing=mono" : "") + " -f \"%{?fullname{%{fullname[0]}}{%{family}}}\\n\"";
    auto f = popen(cmd.c_str(), "r");
    constexpr const int BUFSIZE = 1024*10;
    char buf[BUFSIZE+1];
    while (true) {
        if (fgets(buf, BUFSIZE, f) == nullptr) {
            if (std::feof(f)) {
                break;
            }
            else {
                fclose(f);
                throw std::runtime_error("Error reading from pipe (fc-list)");
            }
        }
        size_t len = strlen(buf);
        if (buf[len-1] == '\n')
            --len;
        if (len > 0 && strncmp(buf, "YuGothic", 8) && strncmp(buf, ".LastResort", 11)) { // ignore YuGothic font, it crashes cairo, .LastResort does not have latin letters
            fonts.emplace_back(buf, len);
        }
    }
    fclose(f);
    std::sort(fonts.begin(), fonts.end());
    fonts.erase(std::unique(fonts.begin(), fonts.end()), fonts.end());
    std::cout << fonts.size() << " fonts available" << std::endl;
    return fonts;

} // get_available_fonts

// ----------------------------------------------------------------------
