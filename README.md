# cairo-fonts
Generates pdf with text in various available fonts.

## Requirements:

- macOS: clang 7.3
- Linux: gcc 5.3 or 6.1
